 docker run -it --rm \
    --volumes-from wordpress \
    --network container:wordpress \
    -e WORDPRESS_DB_USER=root \
    -e WORDPRESS_DB_PASSWORD=password \
    --user 33:33 \
    wordpress:cli sh
