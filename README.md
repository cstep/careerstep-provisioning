## SUMMARY
This repo shows how to provision CareerStep WP website to containers with Nginx and Mysql using wp-cli container for an isolated env and populating/importing plugins and menus with wp-cli commands

## Table of Content
* Requirements
* Before You Begin
* Some Useful Commands
* Some Links
* Spin-UP WP With Nginx and MySQL/MariaDB
* WP-Cli and Installing The Theme
* Import and Export Menus

## Requirements
* Git [latest]
* Docker [latest]
* WP Composer

## Before You Begin
* Please make sure Git installed on host machine
* Please make sure Docker installed on host machine
* WP Composer package will be installed in wp-cli container

## Some Useful Commands
* `docker images`   shows all docker images
* `docker rmi <image>`    removes a docker image

* `docker ps -a`    shows all running and stopped docker containers
* `docker rm -f <container>`    removes a docker container

* `ctrl+pq` helps you to exit from a container without killing it, instead of `ctrl+c`

## Some Links
* https://github.com/mjstealey/wordpress-nginx-docker
* https://github.com/wpbullet/wp-menu-import-export-cli

* https://github.com/hoppinger/advanced-custom-fields-wpcli
* https://github.com/docker-library/wordpress/blob/9300bc2999791d515b3dabdd7cd820f3dca8eebb/cli/php7.4/alpine/Dockerfile

## Spin-Up WP With Nginx and MySQL/MariaDB
```
git clone  https://github.com/mjstealey/wordpress-nginx-docker.git
cd wordpress-nginx-docker
```

**NOTE**: Please refer to README.md in the repo(wordpress-nginx-docker) for more details

```
mkdir -p certs/ certs-data/ logs/nginx/ mysql/ wordpress/
```

```
docker-compose up -d
```

**BROWSE:** localhost

**NOTE:** For TLS, please refer to DOC.md [README.md in the repo(wordpress-nginx-docker)]

## WP-Cli and Installing The Theme
```
git clone git@bitbucket.org:cstep/careerstep-provisioning.git
cd careerstep-provisioning
```

The following command will spin up wp-cli docker container

```
bash wp-cli.sh
```

In order to populate plugins and menus with wp-cli, files and directories needs to be copied into wp-cli container

```
cd
docker cp   /FULL/PATH/TO/careerstep-provisioning   <wp-cli-container>:/var/www/html
```

After copied the repo, we have to go into the container by `docker exec -it <wp-cli-container>  sh`in order to run the needed commands

Please run the following commnands in order
```
cd careerstep-provisioning 
```

```
wp plugin install advanced-custom-fields --activate
wp plugin install wordpress-importer --activate
wp import [import.xml] --authors=create
wp theme install careerstep-provisioning/assets/step-theme.zip --activate
```

**BROWSE:** localhost


## Import and Export Menus
```
cd careerstep-provisioning
```

In order to import and export menus, a composer need to be installed that brings in sub commands for _wp menu_ as **wp menu import** and **wp menu export**. Before that we should run the following commands to create a directory where it will be installed with proper permissions. Please mkdir each folder step by step manually then copy it into wp-cli container and install the package


```
mkdir -p X11/fs/.wp-cli/packages/composer.json
chmod -R 777 X11                        
```

**NOTE:** Since we already have everything ready in the repo, just copy X11 into wp-cli container by the command below (Remember chmod -R 777 X11)

```
docker cp /FULL/PATH/TO/X11  <wp-cli-container>:/etc
docker exec -it  <wp-cli-container>  sh
```

```
wp package install https://github.com/wpbullet/wp-menu-import-export-cli.git
```

```
wp menu import acf-export-2021-06-09.json [--overwrite]

wp export 
```

**BROWSE:** localhost